<?php
namespace KITT3N\Kitt3nLaw\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author Oliver Merz <o.merz@kitt3n.de>
 * @author Georg Kathan <g.kathan@kitt3n.de>
 * @author Dominik Hilser <d.hilser@kitt3n.de>
 */
class CookieTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \KITT3N\Kitt3nLaw\Domain\Model\Cookie
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \KITT3N\Kitt3nLaw\Domain\Model\Cookie();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getDummyReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getDummy()
        );
    }

    /**
     * @test
     */
    public function setDummyForStringSetsDummy()
    {
        $this->subject->setDummy('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'dummy',
            $this->subject
        );
    }
}
