<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'KITT3N.Kitt3nLaw',
            'Kitt3nlawcookierender',
            'CookieConsent Render'
        );

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('kitt3n_law', 'Configuration/TypoScript', 'kitt3n | Law');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_kitt3nlaw_domain_model_cookie', 'EXT:kitt3n_law/Resources/Private/Language/locallang_csh_tx_kitt3nlaw_domain_model_cookie.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_kitt3nlaw_domain_model_cookie');

    }
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder