<?php
namespace KITT3N\Kitt3nLaw\Domain\Model;

/***
 *
 * This file is part of the "kitt3n | Law" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018 Oliver Merz <o.merz@kitt3n.de>
 *           Georg Kathan <g.kathan@kitt3n.de>
 *           Dominik Hilser <d.hilser@kitt3n.de>
 *
 ***/

/**
 * Cookie
 */
class Cookie extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * dummy
     *
     * @var string
     */
    protected $dummy = '';

    /**
     * Returns the dummy
     *
     * @return string $dummy
     */
    public function getDummy()
    {
        return $this->dummy;
    }

    /**
     * Sets the dummy
     *
     * @param string $dummy
     * @return void
     */
    public function setDummy($dummy)
    {
        $this->dummy = $dummy;
    }
}
