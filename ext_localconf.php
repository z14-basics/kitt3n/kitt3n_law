<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'KITT3N.Kitt3nLaw',
            'Kitt3nlawcookierender',
            [
                'Cookie' => 'render'
            ],
            // non-cacheable actions
            [
                'Cookie' => 'render'
            ]
        );

    // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    kitt3nlawcookierender {
                        iconIdentifier = kitt3n_law-plugin-kitt3nlawcookierender
                        title = LLL:EXT:kitt3n_law/Resources/Private/Language/locallang_db.xlf:tx_kitt3n_law_kitt3nlawcookierender.name
                        description = LLL:EXT:kitt3n_law/Resources/Private/Language/locallang_db.xlf:tx_kitt3n_law_kitt3nlawcookierender.description
                        tt_content_defValues {
                            CType = list
                            list_type = kitt3nlaw_kitt3nlawcookierender
                        }
                    }
                }
                show = *
            }
       }'
    );
		$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
		
			$iconRegistry->registerIcon(
				'kitt3n_law-plugin-kitt3nlawcookierender',
				\TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
				['source' => 'EXT:kitt3n_law/Resources/Public/Icons/user_plugin_kitt3nlawcookierender.svg']
			);
		
    }
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder
call_user_func(
    function ($extKey, $globals) {

        // wizards
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
            'mod {
                wizards.newContentElement.wizardItems.plugins.elements.kitt3nlawcookierender >
                wizards.newContentElement.wizardItems {
                    KITT3N {
                        header = KITT3N
                        after = common,special,menu,plugins,forms
                        elements {
                            kitt3nlawcookierender {
                                iconIdentifier = kitt3n_svg_big
                                title = kitt3n | Cookie Notice
                                description = Shows the mandatory cookie notice for the website (DSGVO).
                                tt_content_defValues {
                                    CType = list
                                    list_type = kitt3nlaw_kitt3nlawcookierender
                                }
                            }
                        }
                        show := addToList(kitt3nlawcookierender)
                    }

                }
            }'
        );

    }, $_EXTKEY, $GLOBALS
);