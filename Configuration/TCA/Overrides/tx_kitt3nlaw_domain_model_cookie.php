<?php

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder
defined('TYPO3_MODE') or die();

$sModel = basename(__FILE__, '.php');

// ovr object icon
$GLOBALS['TCA'][$sModel]['ctrl']['iconfile'] = 'EXT:kitt3n_law/Resources/Public/Icons/ico_cookie_16x16.png';