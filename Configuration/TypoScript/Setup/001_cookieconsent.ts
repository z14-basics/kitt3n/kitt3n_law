## Settings for plugin
plugin {
    tx_kitt3nlaw {
        settings {
            cookieconsent {
                active = {$plugin.tx_kitt3nlaw.settings.cookieconsent.active}
                privacy_uid = {$plugin.tx_kitt3nlaw.settings.cookieconsent.privacy_uid}
                privacy_url = {$plugin.tx_kitt3nlaw.settings.cookieconsent.privacy_url}
                popup_color = {$plugin.tx_kitt3nlaw.settings.cookieconsent.popup_color}
                popup_text = {$plugin.tx_kitt3nlaw.settings.cookieconsent.popup_text}
                popup_link = {$plugin.tx_kitt3nlaw.settings.cookieconsent.popup_link}
                button_color = {$plugin.tx_kitt3nlaw.settings.cookieconsent.button_color}
                button_text = {$plugin.tx_kitt3nlaw.settings.cookieconsent.button_text}
                position = {$plugin.tx_kitt3nlaw.settings.cookieconsent.position}
                theme = {$plugin.tx_kitt3nlaw.settings.cookieconsent.theme}
                overlay = {$plugin.tx_kitt3nlaw.settings.cookieconsent.overlay}
            }
            tracking {
                # Activate Google Analytics Tracking (0 if not used)
                ga = {$plugin.tx_kitt3nlaw.settings.tracking.ga}
                # Google Analytics property ID
                ga_id = {$plugin.tx_kitt3nlaw.settings.tracking.ga_id}

                # Activate Piwik/Matomo Tracking (0 if not used)
                pk = {$plugin.tx_kitt3nlaw.settings.tracking.pk}
                # Piwik/Matomo URL
                pk_url = {$plugin.tx_kitt3nlaw.settings.tracking.pk_url}
                # Piwik/Matomo site ID
                pk_id = {$plugin.tx_kitt3nlaw.settings.tracking.pk_id}

                # Activate eTracker Tracking (0 if not used)
                et = {$plugin.tx_kitt3nlaw.settings.tracking.et}
                # eTracker configurations
                et_secure_code = {$plugin.tx_kitt3nlaw.settings.tracking.et_secure_code}
                et_pagename = {$plugin.tx_kitt3nlaw.settings.tracking.et_pagename}
                et_areas = {$plugin.tx_kitt3nlaw.settings.tracking.et_areas}
                et_url = {$plugin.tx_kitt3nlaw.settings.tracking.et_url}
                et_target = {$plugin.tx_kitt3nlaw.settings.tracking.et_target}
                et_tval = {$plugin.tx_kitt3nlaw.settings.tracking.et_tval}
                et_tonr = {$plugin.tx_kitt3nlaw.settings.tracking.et_tonr}
                et_tsale = {$plugin.tx_kitt3nlaw.settings.tracking.et_tsale}
                et_basket = {$plugin.tx_kitt3nlaw.settings.tracking.et_basket}
                et_cust = {$plugin.tx_kitt3nlaw.settings.tracking.et_cust}
                et_easy = {$plugin.tx_kitt3nlaw.settings.tracking.et_easy}
                et_se = {$plugin.tx_kitt3nlaw.settings.tracking.et_se}
            }
        }
    }
}

## Include basic resources from cloudflare (not locally)
#[(isLoaded('kitt3n_law')) && ({$plugin.tx_kitt3nlaw.settings.cookieconsent.active} > 0)]
#    page {
#        includeCSSLibs {
#            cookieconsent = https://cdnjs.cloudflare.com/ajax/libs/cookieconsent2/{$plugin.tx_kitt3nlaw.settings.cookieconsent.version}/cookieconsent.min.css
#            cookieconsent.external = 1
#        }
#        includeJSLibs {
#            cookieconsent = https://cdnjs.cloudflare.com/ajax/libs/cookieconsent2/{$plugin.tx_kitt3nlaw.settings.cookieconsent.version}/cookieconsent.min.js
#            cookieconsent.external = 1
#            cookieconsent.async = 1
#        }
#    }
#[global]


## Merge the plugin in a lib
lib.tx_kitt3nlaw_cookieconsent_render = COA
lib.tx_kitt3nlaw_cookieconsent_render {
    10 = USER
    10 {
        userFunc = TYPO3\CMS\Extbase\Core\Bootstrap->run
        extensionName = Kitt3nLaw
        pluginName = Kitt3nlawcookierender
        vendorName = KITT3N
        controller = Cookie
        action = render
        settings =< plugin.tx_kitt3nlaw.settings
        persistence =< plugin.tx_kitt3nlaw_kitt3nlawcookierender.persistence
        view =< plugin.tx_kitt3nlaw_kitt3nlawcookierender.view
    }
}

## Insert plugin into page template
[(isLoaded('kitt3n_law')) && ({$plugin.tx_kitt3nlaw.settings.cookieconsent.active} > 0)]
    page = PAGE
    page {
        # add a fixed plugin to be rendered after the normal fluid template
        20 = USER
        20 < lib.tx_kitt3nlaw_cookieconsent_render
    }
[END]

## Include Google Analytics if cookie is set
[(isLoaded('kitt3n_law')) && ({$plugin.tx_kitt3nlaw.settings.cookieconsent.active} > 0) && ({$plugin.tx_kitt3nlaw.settings.tracking.ga} > 0) && (like(request.getCookieParams()['kitt3nlawcookieconsent'], 'allow'))]
    page.headerData.214003 = TEXT
    page.headerData.214003.value (
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id={$plugin.tx_kitt3nlaw.settings.tracking.ga_id}"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', '{$plugin.tx_kitt3nlaw.settings.tracking.ga_id}', { 'anonymize_ip': true });
</script>
    )
[END]

## Include Matomo/Piwik if cookie is set
[(isLoaded('kitt3n_law')) && ({$plugin.tx_kitt3nlaw.settings.cookieconsent.active} > 0) && ({$plugin.tx_kitt3nlaw.settings.tracking.pk} > 0) && (like(request.getCookieParams()['kitt3nlawcookieconsent'], 'allow'))]
    page.headerData.214004 = TEXT
    page.headerData.214004.value (
<!-- Matomo -->
<script type="text/javascript">
  var _paq = _paq || [];
  /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
  _paq.push(['trackPageView']);
  _paq.push(['enableLinkTracking']);
  (function() {
    var u="{$plugin.tx_kitt3nlaw.settings.tracking.pk_url}";
    _paq.push(['setTrackerUrl', u+'piwik.php']);
    _paq.push(['setSiteId', '{$plugin.tx_kitt3nlaw.settings.tracking.pk_id}']);
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
    g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
  })();
</script>
<!-- End Matomo Code -->
    )
[END]

## Include etracker if cookie is set
[(isLoaded('kitt3n_law')) && ({$plugin.tx_kitt3nlaw.settings.cookieconsent.active} > 0) && ({$plugin.tx_kitt3nlaw.settings.tracking.et} > 0) && (like(request.getCookieParams()['kitt3nlawcookieconsent'], 'allow'))]
    page.headerData.214005 = TEXT
    page.headerData.214005.value (
<!-- Copyright (c) 2000-2016 etracker GmbH. All rights reserved. -->
<!-- This material may not be reproduced, displayed, modified or distributed -->
<!-- without the express prior written permission of the copyright holder. -->
<!-- etracker tracklet 4.1 -->
<script type="text/javascript">
    var et_pagename = "{$plugin.tx_kitt3nlaw.settings.tracking.et_pagename}";
    var et_areas = "{$plugin.tx_kitt3nlaw.settings.tracking.et_areas}";
    var et_url = "{$plugin.tx_kitt3nlaw.settings.tracking.et_url}";
    var et_target = "{$plugin.tx_kitt3nlaw.settings.tracking.et_target}";
    var et_tval = "{$plugin.tx_kitt3nlaw.settings.tracking.et_tval}";
    var et_tonr = "{$plugin.tx_kitt3nlaw.settings.tracking.et_tonr}";
    var et_tsale = "{$plugin.tx_kitt3nlaw.settings.tracking.et_tsale}";
    var et_basket = "{$plugin.tx_kitt3nlaw.settings.tracking.et_basket}";
    var et_cust = "{$plugin.tx_kitt3nlaw.settings.tracking.et_cust}";
    var et_easy = "{$plugin.tx_kitt3nlaw.settings.tracking.et_easy}";
    var et_se = "{$plugin.tx_kitt3nlaw.settings.tracking.et_se}";
</script>
<script id="_etLoader" type="text/javascript" charset="UTF-8" data-secure-code="{$plugin.tx_kitt3nlaw.settings.tracking.et_secure_code}" src="//static.etracker.com/code/e.js"></script>
<!-- etracker tracklet 4.1 end -->
    )
[END]